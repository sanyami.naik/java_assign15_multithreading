package Assignments.Fourteen;



public class ThreadMethods implements Runnable{

    public void run()
    {
        {

        }

    }

    public static void main(String[] args) throws InterruptedException {

        System.out.println("The main thread "+Thread.currentThread());

        ThreadDemo threadDemo1=new ThreadDemo();
        ThreadDemo threadDemo2=new ThreadDemo();
        ThreadDemo threadDemo3=new ThreadDemo();

        Thread t1=new Thread(threadDemo1);
        System.out.println("The name of the thread is "+t1.getName());
        System.out.println("The state of thread is "+t1.getState());
        System.out.println("The thread is alive or not "+t1.isAlive());
        t1.setName("Sanyami");
        System.out.println("The new name of the thread is "+t1.getName());
        t1.setDaemon(true);
        System.out.println("The thread is daemon "+t1.isDaemon());




        Thread t2=new Thread(threadDemo2);
        System.out.println("The name of the thread is "+t2.getName());
        System.out.println("The state of thread is "+t2.getState());
        System.out.println("The thread is alive or not "+t2.isAlive());
        System.out.println("The thread is daemon "+t2.isDaemon());



        Thread t3=new Thread(threadDemo3);
        System.out.println("The name of the thread is "+t3.getName());
        System.out.println("The state of thread is "+t3.getState());
        System.out.println("The thread is alive or not "+t3.isAlive());
        System.out.println("The thread is daemon "+t3.isDaemon());




    }

}

/*OUTPUT
The main thread Thread[main,5,main]
The name of the thread is Thread-3
The state of thread is NEW
The thread is alive or not false
The new name of the thread is Sanyami
The thread is daemon true
The name of the thread is Thread-4
The state of thread is NEW
The thread is alive or not false
The thread is daemon false
The name of the thread is Thread-5
The state of thread is NEW
The thread is alive or not false
The thread is daemon false
 */