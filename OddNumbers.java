package Assignments.Fourteen;


public class OddNumbers extends Thread {

    static void findOdd(int bound) {
        int i = 1;
        while (i<bound) {
            if (i % 2 != 0)
                System.out.println(i);

            i++;
        }
    }

    public void run ()
    {
        findOdd(20);
    }



    public static void main(String[] args)
    {
        OddNumbers oddNumbers=new OddNumbers();
        oddNumbers.start();

    }
}


/*OUTPUT
1
3
5
7
9
11
13
15
17
19
 */
