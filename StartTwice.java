package Assignments.Fourteen;

class ThreadDemo extends Thread{

    public void run()
    {
        System.out.println("The current thread is "+Thread.currentThread());
        System.out.println("The status of thread1 is "+getState());
    }
    void create()
    {
        System.out.println("The current thread is "+Thread.currentThread());
        ThreadDemo thread=new ThreadDemo();
        System.out.println("The status of thread1 is "+thread.getState());
        thread.start();
        System.out.println("The status of thread1 is "+thread.getState());
        thread.start();

    }

}



public class StartTwice {
    public static void main(String[] args) {

        ThreadDemo threadDemo = new ThreadDemo();
        threadDemo.create();
    }

}


/*OUTPUT
The current thread is Thread[main,5,main]
The status of thread1 is NEW
The status of thread1 is RUNNABLE
The current thread is Thread[Thread-1,5,main]
The status of thread1 is RUNNABLE
Exception in thread "main" java.lang.IllegalThreadStateException
	at java.lang.Thread.start(Thread.java:710)
	at Assignments.Fourteen.ThreadDemo.create(StartTwice.java:17)
	at Assignments.Fourteen.StartTwice.main(StartTwice.java:29)
 */



/*
OUTPUT:
In java a thread can never be restarted again if its dead.

The current thread is Thread[main,5,main]
The status of thread1 is NEW
The status of thread1 is RUNNABLE
The current thread is Thread[Thread-1,5,main]
The status of thread1 is RUNNABLE
Exception in thread "main" java.lang.IllegalThreadStateException
	at java.lang.Thread.start(Thread.java:710)
	at Assignments.Fourteen.ThreadDemo.create(StartTwice.java:17)
	at Assignments.Fourteen.StartTwice.main(StartTwice.java:29)
 */