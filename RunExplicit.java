package Assignments.Fourteen;


class ThreadDemoo extends Thread{

    public void run()
    {
        System.out.println("The current thread is "+Thread.currentThread());
        System.out.println("The status of thread1 is "+getState());
    }
    void create()
    {
        System.out.println("The current thread is "+Thread.currentThread());
        ThreadDemo thread=new ThreadDemo();
        System.out.println("The status of thread1 is "+thread.getState());
        thread.start();
        thread.run();               //The run method is called explicitly threading does not occur it just calls the instance method of class


    }

}


public class RunExplicit {
    public static void main(String[] args) {

        ThreadDemoo threadDemoo = new ThreadDemoo();
        threadDemoo.create();


    }
}




/*OUTPUT
The current thread is Thread[main,5,main]
The status of thread1 is NEW
The current thread is Thread[main,5,main]
The status of thread1 is RUNNABLE
The current thread is Thread[Thread-1,5,main]
The status of thread1 is RUNNABLE

It means the run method when called explicitly wont be able to create another thread.That wil be called on the main frame stack
 */



/*
OUTPUT:
The current thread is Thread[main,5,main]
The status of thread1 is NEW
The current thread is Thread[main,5,main]
The status of thread1 is RUNNABLE
The current thread is Thread[Thread-1,5,main]
The status of thread1 is RUNNABLE
 */